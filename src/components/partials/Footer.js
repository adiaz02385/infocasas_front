import React from 'react';
 
 
class Footer extends React.Component {
 
	render() {
 
		return (
			<>
				<hr className="featurette-divider" />
				<footer className="container text-center">
						<p>&copy; {(new Date().getFullYear())} Gestor de tareas. &middot; <a href="mailto:adiaz02385@gmail.com">adiaz02385@gmail.com</a></p>
				</footer>
			</>
		)
		
	}
 
}
 
export default Footer;