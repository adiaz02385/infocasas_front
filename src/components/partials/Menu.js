import React from 'react';
import {NavLink} from "react-router-dom";

class Menu extends React.Component {

    render() {

  	return (

		<nav className="navbar navbar-light bg-light">
		<div className="container-fluid">
			<NavLink to="/nueva" className="btn btn-success">Nueva Tarea</NavLink>			
			<NavLink to="/" className="navbar-brand">Gestor de Tareas</NavLink>
		</div>
		</nav>
  		

  	)
    
  }

}

export default Menu;