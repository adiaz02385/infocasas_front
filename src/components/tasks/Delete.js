import React, { Component } from 'react';
import axios from 'axios';
import Menu from '../partials/Menu';
import Footer from '../partials/Footer';
import {API_URL} from '../../Constans';
import Swal from 'sweetalert2';

class Delete extends Component {

    constructor(props){
        super(props);
        const id = props.location.dataTask.id;
        this.state = {
            id : id,
            name : props.location.dataTask.name,
            completed : props.location.dataTask.completed,
            difficulty : props.location.dataTask.difficulty
        }

    }

   

    handleSubmit = (e) => {
    e.preventDefault();
        const packets = {
            name:  this.state.name,
            completed: this.state.completed,
            difficulty: this.state.difficulty,
            id: this.state.id
        };
        axios.post(`${API_URL}task/delete/${packets.id}`, packets)
            .then(
                response => {
                    Swal.fire('Buen trabajo!',
                    'Tarea eliminada!',
                    'success').then(function() {
                        window.location = "/";
                    });
                }
                )
            .catch(error => {
//                console.log(error.response.data)
                Swal.fire({
                    icon: 'error',
                    text: 'Opps...ocurrió un error, intente nuevamente'
                    })
                });
    }
    render(){
        
        return (
            <>
            <Menu />
            
            <main role="main" className="flex-shrink-0 mt-5">
    
                <div className="container">
                    <h1>¿Está seguto que desea eliminar la tarea?</h1> 
                    <p>Nombre: {this.state.name}</p> 
                    <p>Estatus: {this.state.completed === 0 ? 'Pendiente' : 'Completado'}</p> 
                    <form className="mb-5">
                        <input type="submit" className="btn btn-danger" onClick={this.handleSubmit} value="Eliminar"/>
                    </form>
                        
                </div>
    
              </main>
    
              <Footer />
    
            </>

        );
    }
}
export default Delete;