import React, { Component } from 'react';
import axios from 'axios';
import Menu from '../partials/Menu';
import Footer from '../partials/Footer';
import {API_URL} from '../../Constans';
import Swal from 'sweetalert2';

class Edit extends Component {

    constructor(props){
        super(props);
        const id = props.location.dataTask.id;
        this.state = {
            id : id,
            name : props.location.dataTask.name,
            completed : props.location.dataTask.completed,
            difficulty : props.location.dataTask.difficulty
        }

        this.name = this.name.bind(this);
        this.completed = this.completed.bind(this);
        this.difficulty = this.difficulty.bind(this);

    }

    name(event){
        this.setState({name : event.target.value})
    }
    completed(event){
        this.setState({completed : event.target.value})
    }
    difficulty(event){
        this.setState({difficulty : event.target.value})
    }

    handleSubmit = (e) => {
    e.preventDefault();
        const packets = {
            name:  this.state.name,
            completed: this.state.completed,
            difficulty: this.state.difficulty,
            id: this.state.id
        };
        axios.post(`${API_URL}task/update/${packets.id}`, packets)
            .then(
                response => {
                    Swal.fire('Buen trabajo!',
                    'Tarea actualizada!',
                    'success').then(function() {
                        window.location = "/";
                    });
                }
                )
            .catch(error => {
//                console.log(error.response.data)
                Swal.fire({
                    icon: 'error',
                    text: 'Por favor, llene el formulario'
                    })
                });
    }
    render(){
        
        return (
            <>
            <Menu />
            
            <main role="main" className="flex-shrink-0 mt-5">
    
                <div className="container">
                    <h1>Nueva Tarea</h1> 
                    <form className="mb-5">
    
                        <div className="form-group my-3">
                            <label htmlFor="name" className="fw-bold">Nombre</label>
                            <input type="text" value={this.state.name} onChange={this.name}  className="form-control" id="name" required />            
                        </div>
    
                        <div className="form-group my-3">
                            <label htmlFor="estatus" className="fw-bold">Estatus</label>
                            <div className="form-check">
                                <input className="form-check-input" defaultChecked={this.state.completed === 0}  onChange={this.completed} type="radio" name="completed" id="completed1" value="0"  />
                                <label className="form-check-label" htmlFor="completed1">
                                    Pendiente
                                </label>
                            </div>
                            <div className="form-check my-3">
                                <input className="form-check-input" defaultChecked={this.state.completed === 1}  onChange={this.completed} type="radio" name="completed" id="completed2" value="1"  />
                                <label className="form-check-label" htmlFor="completed2">
                                    Completado
                                </label>
                            </div>
                        </div>
    
                        <div className="form-group my-3">
                            <label htmlFor="difficulty" className="fw-bold">Dificultad</label>
                            <select className="form-select" defaultValue={this.state.difficulty} onChange={this.difficulty} >
                                <option value="easy">Fácil</option>
                                <option value="medium">Media</option>
                                <option value="hard">Compleja</option>
                            </select>
                        </div>
    
                        <input type="submit" className="btn btn-primary" onClick={this.handleSubmit} value="Registrar"/>
    
                    </form>
                        
                </div>
    
              </main>
    
              <Footer />
    
            </>

        );
    }
}
export default Edit;