import React, { useEffect, useState } from 'react';
import axios from 'axios';
import moment from 'moment';
import 'moment/locale/es';
import { NavLink } from "react-router-dom";

import { API_URL } from '../../Constans'
import Menu from '../partials/Menu';
import Footer from '../partials/Footer';

export const Tasks = () => {
    const [listTask, setlistTask] = useState([]);
    const [searchText, setSearchText] = useState('');
    useEffect(() => {
        axios
            .get(`${API_URL}task/`, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
            .then((res) => {
                setlistTask(res.data.task);
            })
            .catch((err) => {
                console.log(err);
            });
    }, []);

    const lista = listTask.map(task => (

        <tr key={task.id}>
            <td>{task.id}</td>
            <td><NavLink to={{
                pathname: "/editar/" + task.id,
                dataTask: task
            }} >{task.name}</NavLink></td>
            <td>{task.completed === 0 ? 'Pendiente' : 'Completado'}</td>
            <td>{task.difficulty === 'easy' ? 'Fácil' : task.difficulty === 'medium' ? 'Media' : 'Compleja'}</td>
            <td>{moment(task.updated_at).format('Do MMMM YYYY, h:mm:ss a')}</td>
            <td>
                <NavLink className="btn btn-outline-warning" to={{
                    pathname: "/editar/" + task.id,
                    dataTask: task
                }} >Editar</NavLink>
                <NavLink className="btn btn-outline-danger mx-2" to={{
                    pathname: "/eliminar/" + task.id,
                    dataTask: task
                }} >Eliminar</NavLink>
            </td>
        </tr>
    )
    );

    const handleFilter = (status) => {
        //Consultar
        const url = ( status !== 'all') ? `${API_URL}task/status/${status}` : `${API_URL}task`;  
        axios
            .get(url, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
            .then((res) => {
                setlistTask(res.data.task);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    const handleSearch = () => {
        axios
            .get(`${API_URL}task/buscar/${searchText}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'X-Requested-With': 'XMLHttpRequest'
                }
            })
            .then((res) => {
                console.log(searchText);
                setlistTask(res.data.task);
            })
            .catch((err) => {
                console.log(err);
            });
    }

    return (
        <>
            <Menu />

            <main role="main" className="flex-shrink-0 mt-5">
                <div className="container">
                    <div className="row">
                        <div className="offset-lg-6 col-lg-6 my-2">
                            <form className="d-flex">
                                <input className="form-control me-2" name="termino" onChange={(e) => setSearchText(e.target.value)} id="termino" type="text" placeholder="Buscar por nombre" aria-label="Buscar" />
                                <input type="button" onClick={handleSearch} className="btn btn-primary" value="Buscar"/>
                            </form>
                        </div>
                    </div>
                    <h1>Listado de Tareas</h1>
                    <div className="row">
                        <div className="col-lg-6">
                            <form className="d-flex">
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input"  onClick={(e) => handleFilter(e.target.value)} type="radio" name="inlineRadioOptions" id="inlineRadio1" value="all" defaultChecked />
                                    <label className="form-check-label" htmlFor="inlineRadio1">Todas</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input"  onClick={(e) => handleFilter(e.target.value)} type="radio" name="inlineRadioOptions" id="inlineRadio2" value="completed" />
                                    <label className="form-check-label" htmlFor="inlineRadio2">Completadas</label>
                                </div>
                                <div className="form-check form-check-inline">
                                    <input className="form-check-input"  onClick={(e) => handleFilter(e.target.value)} type="radio" name="inlineRadioOptions" id="inlineRadio3" value="pending"  />
                                    <label className="form-check-label" htmlFor="inlineRadio3">Pendientes</label>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table className="table table-responsive">
                        <thead className="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Estatus</th>
                                <th scope="col">Dificultad</th>
                                <th scope="col">Actualizada el</th>
                                <th scope="col">Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            {lista}
                        </tbody>
                    </table>
                </div>
            </main>

            <Footer />

        </>
    )
}