import React from "react";
import ReactDOM from 'react-dom';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";

import {Tasks} from './components/tasks/Index';
import Edit from './components/tasks/Edit';
import Add from "./components/tasks/Add";
import Delete from "./components/tasks/Delete";

import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import '../node_modules/bootstrap/dist/js/bootstrap.min.js';


ReactDOM.render(
  
 <Router>
	    <div>
	    	<Switch>

		        {/* Páginas */}
		        <Route exact path='/' component={Tasks} />
		        <Route path='/editar/:id' component={Edit} />
		        <Route path='/nueva' component={Add} />
		        <Route path='/eliminar/:id' component={Delete} />

	      	</Switch>
	    </div>
  </Router>,
  document.getElementById('root')
);